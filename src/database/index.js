const Sequelize = require("sequelize");

const configDatabase = require("../config/database");
const Recipes = require("../app/models/Recipes");

require('dotenv/config');

const models = [Recipes];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(configDatabase);

    models.map((model) => model.init(this.connection));
  }
}

module.exports = new Database();
