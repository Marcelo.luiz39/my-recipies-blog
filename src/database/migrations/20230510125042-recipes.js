"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("recipes", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      categorias: {
        type: Sequelize.STRING,
        allowNull: false,
       
      },

      ingredientes: {
        type: Sequelize.TEXT,
        allowNull: false,
       
      },

      preparo: {
        type: Sequelize.TEXT,
        allowNull: false,
      },

      tempo_preparo: {
        type: Sequelize.STRING,
        defaultValue: false,
        allowNull: false,
      },

      notas: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },

      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("recipes");
  },
};
