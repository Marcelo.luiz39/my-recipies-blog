const routes = require("express").Router();

const RecipesController = require("./app/controllers/RecipesController");

routes.post("/api/recipes", RecipesController.store);

routes.get("/api/recipes", RecipesController.index);

routes.get("/api", (req, res) => {
  return res.json({ message: "Hello World" });
});

module.exports = routes;
