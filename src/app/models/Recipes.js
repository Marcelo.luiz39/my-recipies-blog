const Sequelize = require("sequelize");
const { Model } = require("sequelize");

class Recipes extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: Sequelize.STRING,
          allowNull: false,

        },
        categorias: {
          type: Sequelize.STRING,

        },
        ingredientes: {
          type: Sequelize.TEXT,
        },
        preparo: {
          type: Sequelize.TEXT,

        },
        tempo_preparo: {
          type: Sequelize.STRING,
        },
        notas: {
          type: Sequelize.STRING,
        },
      },
      {
        sequelize,
      }
    );
  }
}

module.exports = Recipes;
