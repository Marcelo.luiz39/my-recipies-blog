const Recipes = require("../models/Recipes");
const { v4 } = require("uuid");
const { object, string } = require("yup");

class RecipesController {
  async store(req, res) {
    const userSchema = object().shape({
      name: string().required(),
      categorias: string().required(),
      ingredientes: string().required(),
      preparo: string().required(),
      tempo_preparo: string().required(),
      notas: string(),
    });

    try {
      userSchema.validateSync(req.body, { abortEarly: false });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ error: err.errors });
    }

    const { name, categorias, ingredientes, preparo, tempo_preparo, notas } =
      req.body;

    const recipes = await Recipes.create({
      id: v4(),
      name,
      categorias,
      ingredientes,
      preparo,
      tempo_preparo,
      notas,
    });

    return res.status(201).json({
      id: recipes.id,
      name: recipes.name,
      categorias: recipes.categorias,
      ingredientes: recipes.ingredientes,
      preparo: recipes.preparo,
      tempo_preparo: recipes.tempo_preparo,
      notas: recipes.notas,
    });
  }

  async index(req, res) {
    if (Recipes) {
      Recipes.findAll().then((recipes) => {
        const recipeIngredients = {};
        const recipePreparation = {};

        recipes.forEach((recipe) => {
          const ingredients = recipe.ingredientes.split(",");
          recipeIngredients[recipe.name] = ingredients.map((i) => i.trim());
        });

        console.log("Lista de ingredientes por receita:");
        Object.entries(recipeIngredients).forEach(
          ([recipeName, ingredients]) => {
            console.log(`${recipeName}:`);
            ingredients.forEach((ingredient, index) => {
              console.log(`${index + 1}. ${ingredient}`);
            });
            console.log("");
          }
        );

        recipes.forEach((recipe) => {
          const preparo = recipe.preparo.split(",");
          recipePreparation[recipe.name] = preparo.map((i) => i.trim());
        });

        console.log("Lista de preparo por receita:");
        Object.entries(recipePreparation).forEach(([recipeName, preparo]) => {
          console.log(`${recipeName}:`);
          preparo.forEach((preparo, index) => {
            console.log(`${index + 1}. ${preparo}`);
          });
          console.log("");
        });

        const recipeTime = {};
        recipes.forEach((recipe) => {
          const tempo_preparo = recipe.tempo_preparo.split(",");
          recipeTime[recipe.name] = tempo_preparo.map((i) => i.trim());
        });

        console.log("Lista de tempo de preparo por receita:");
        Object.entries(recipeTime).forEach(([recipeName, tempo_preparo]) => {
          console.log(`${recipeName}:`);
          console.log(`${tempo_preparo} minutos`);
          console.log("");
        });

        return res.status(200).json(Recipes);
      });
    }
    return res.status(400).json({ error: "Não há receitas cadastradas" });
  }
}

module.exports = new RecipesController();
